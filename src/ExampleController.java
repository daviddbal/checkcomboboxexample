import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.VBox;

import org.controlsfx.control.CheckComboBox;

public class ExampleController {
    @FXML VBox myVBox;
    @FXML ComboBox<String> myComboBox;
    CheckComboBox<String> myChoiceComboBox;
    
    public void initialize() {
        final ObservableList<String> strings = FXCollections.observableArrayList();
        for (int i = 0; i <= 10; i++) {
            strings.add("Item " + i);
        }
        myComboBox.setItems(strings);
        myChoiceComboBox = new CheckComboBox<String>(strings);
        myVBox.getChildren().add(myChoiceComboBox);

    }
    
}
