
import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class CheckComboBoxTest extends Application {
    
@Override
  public void start(Stage primaryStage) throws IOException {

    VBox root = FXMLLoader.load(getClass().getResource("Main.fxml"));
    primaryStage.setTitle("CheckComboBox Test");
    primaryStage.setScene(new Scene(root, 300, 275));
    primaryStage.show();
}

  public static void main(String[] args) {
    launch(args);
  }

}