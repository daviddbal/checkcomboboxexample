

CheckComboBox doesn't have a setItems method like the regular ComboBox has. This deficiency prevents CheckComboBox from being used in FXML files.

I made an SSCCE example that demonstrates the difference between the ComboBox and the CheckComboBox with FXML. Below is the bitbucket repository.

https://daviddbal@bitbucket.org/daviddbal/checkcomboboxexample.git

How hard would it be to add a setItems method? It seems trivial, but maybe I'm missing something. Was it omitted on purpose for a good reason? Is there another way to use CheckComboBox with FXML files?